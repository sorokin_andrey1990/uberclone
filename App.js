import React, {useEffect} from 'react';
import {ScaledSheet} from 'react-native-size-matters';
import {NavigationContainer} from '@react-navigation/native';
import Main from 'views/Main';
import {observer} from 'mobx-react';
import {requestLocationPermission} from 'services';
import {Platform} from 'react-native';

const App = () => {
  return (
    <NavigationContainer>
      <Main />
    </NavigationContainer>
  );
};

export default observer(App);

const styles = ScaledSheet.create({
  container: {},
});
