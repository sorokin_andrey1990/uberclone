const en = {
  HomePage: {
    autoCompletePlaceholderOrigin: 'Enter pick up location',
    autoCompletePlaceholderDestination: 'Enter drop off location',
    pickRide: 'Get a ride',
    delivery: 'Get a delivery',
  },
};

export default en;
