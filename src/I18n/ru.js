const ru = {
  HomePage: {
    autoCompletePlaceholderOrigin: 'Введите точку отправления',
    autoCompletePlaceholderDestination: 'Введите точку назначения',
    pickRide: 'Заказать такси',
    delivery: 'Доставка еды',
  },
};

export default ru;
