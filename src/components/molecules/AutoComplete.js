import React, {useState, useLayoutEffect, useRef, useEffect} from 'react';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {ScaledSheet} from 'react-native-size-matters';
import {observer} from 'mobx-react';
import Geolocation from 'react-native-geolocation-service';
import {useTranslation} from 'react-i18next';
import {getAddressFromCoordinates} from 'services';
import {Alert, Platform} from 'react-native';
import {requestLocationPermission} from 'services';

// important for current location to work propertly
navigator.geolocation = require('react-native-geolocation-service');

const AutoComplete = observer(
  ({placeholder, handleAutoComplete, currentLocationSet, onPress}) => {
    const [reverseGeocode, setReverseGeocode] = useState({});
    const [address, setAddress] = useState('');
    const fieldValue = useRef();

    useLayoutEffect(() => {
      Platform.OS === 'android' && requestLocationPermission();
      currentLocationSet &&
        Geolocation.getCurrentPosition(
          position => {
            return (
              setReverseGeocode({
                lat: position.coords.latitude,
                lng: position.coords.longitude,
              }),
              currentLocationSet
                ? currentLocationSet({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                  })
                : null
            );
          },
          error => {
            console.log(error);
          },
        );
    }, []);

    useEffect(() => {
      getAddressFromCoordinates(setAddress, reverseGeocode);
      currentLocationSet ? fieldValue.current.setAddressText(address) : null;
    });

    return (
      <GooglePlacesAutocomplete
        ref={fieldValue}
        currentLocation
        enableHighAccuracyLocation={true}
        placeholder={placeholder}
        enablePoweredByContainer={false}
        minLength={2}
        fetchDetails={true}
        styles={{
          container: {
            flex: 0,
          },
        }}
        onPress={(data, details = null) => {
          onPress ? onPress() : null;
          handleAutoComplete(data, details);
        }}
        query={{
          key: 'AIzaSyBv6C_P04-lb5dC9D0pD6170eR3ah0tpSo',
          language: 'en',
          // components: 'country:kz',
        }}
        nearbyPlacesAPI="GooglePlacesSearch"
        debounce={400}
      />
    );
  },
);

export default AutoComplete;

const styles = ScaledSheet.create({
  container: {},
});
