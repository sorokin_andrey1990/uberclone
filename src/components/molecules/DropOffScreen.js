import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import AutoComplete from 'molecules/AutoComplete';
import {useTranslation} from 'react-i18next';
import {handleAutoComplete} from 'services';
import {useNavigation} from '@react-navigation/native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

const DropOffScreen = () => {
  const {t} = useTranslation();
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <AutoComplete
        placeholder={t('HomePage.autoCompletePlaceholderDestination')}
        handleAutoComplete={handleAutoComplete.setDestination}
        onPress={() => navigation.navigate('RidePickUp')}
      />
    </View>
  );
};

export default DropOffScreen;

const styles = ScaledSheet.create({
  container: {},
});
