import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  Alert,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import homeButtons from '../../mock/home';
import {SIZES} from 'constants';
import {useNavigation} from '@react-navigation/native';
import {useTranslation} from 'react-i18next';
import Locations from 'store/locations';

const HomeButtons = () => {
  const navigation = useNavigation();
  const {t} = useTranslation();
  const buttons = homeButtons();
  return (
    <View style={styles.btnContainer}>
      <FlatList
        horizontal
        data={buttons}
        renderItem={({item, index}) => {
          return (
            <TouchableOpacity
              onPress={() => {
                index == 0 && Locations.origin
                  ? navigation.navigate('MapScreen')
                  : index == 1
                  ? Alert.alert('Notification', 'Not implemented yet')
                  : Alert.alert(
                      'Notification',
                      'You need to specify pickup location',
                    );
              }}
              activeOpacity={0.7}
              style={[styles.btnWrap, {marginRight: index == 0 ? 10 : 0}]}>
              <Image style={styles.btnImage} source={item.image} />
              <Text style={styles.btnTitle}>{item.title}</Text>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default HomeButtons;

const styles = ScaledSheet.create({
  btnContainer: {
    zIndex: 1,
    alignItems: 'center',
  },
  btnWrap: {
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: '25@ms',
    width: '150@ms',
    height: '180@ms',
    backgroundColor: '#e1e1e1',
    borderRadius: SIZES.radius5,
    padding: SIZES.padding10,
  },

  btnImage: {
    width: '100%',
    height: '100@ms',
    resizeMode: 'contain',
  },
  btnTitle: {
    marginTop: SIZES.margin5,
    fontSize: SIZES.titleText,
  },
});
