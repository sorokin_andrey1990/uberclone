import React, {useState, useLayoutEffect, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Rides from 'mock/ridePickUp';
import ArrowLeft from 'assets/arrowLeft';
import {useNavigation} from '@react-navigation/native';
import Distance from 'services/distanceHandle';
import Locations from 'store/locations';
import {toJS} from 'mobx';
import {observer} from 'mobx-react';
import RidePrices from 'services/ridePrices';
import {screenHeight} from 'constants/theme';

const RidePickUp = observer(() => {
  const [active, setActive] = useState(null);
  const navigation = useNavigation();
  const [basePriceDaily, setBasePriceDaily] = useState(0);
  const [basePriceComfort, setBasePriceComfort] = useState(0);
  const [basePriceBusiness, setBasePriceBusiness] = useState(0);
  const discountDaily = null;
  const discountComfort = 7;
  const discountBusiness = 11;
  const [promoPrice, setPromoPrice] = useState(null);

  useEffect(() => {
    Distance();
    setBasePriceDaily(RidePrices.getDailyPrice(Locations.distance));
    setBasePriceComfort(
      RidePrices.getComfortPrice(Locations.distance).toFixed(0),
    );
    setBasePriceBusiness(
      RidePrices.getBusinessPrice(Locations.distance).toFixed(0),
    );
  }, [Locations.origin, Locations.destination, Locations.distance]);
  console.log(toJS(Locations));
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollWrap}>
        <View style={styles.headerWrap}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <ArrowLeft style={{width: '5%'}} />
          </TouchableOpacity>
          <Text style={styles.headerTitle}>Select your ride</Text>
        </View>
        {Rides.map(item => {
          return (
            <TouchableOpacity
              key={item.id}
              style={{
                backgroundColor: item.id == active?.id ? '#DAF7A6' : '#fff',
                marginVertical: 5,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderRadius: 5,
              }}
              onPress={() => setActive(item)}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  resizeMode="contain"
                  style={{width: 90, height: 80, marginRight: 15}}
                  source={item.image}
                />
                <View>
                  <Text style={{fontSize: 18, fontWeight: '700'}}>
                    {item.title}
                  </Text>
                  <Text>
                    {Locations.duration} minutes - {Locations.distance} kms
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'column'}}>
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: '700',
                    marginRight: 15,
                    textDecorationLine:
                      item.title === 'Daily'
                        ? discountDaily
                          ? 'line-through'
                          : 'none'
                        : item.title === 'Comfort'
                        ? discountComfort
                          ? 'line-through'
                          : 'none'
                        : item.title === 'Business'
                        ? discountBusiness
                          ? 'line-through'
                          : 'none'
                        : 'none',
                  }}>
                  {item.title === 'Daily' && basePriceDaily}
                  {item.title === 'Comfort' && basePriceComfort}
                  {item.title === 'Business' && basePriceBusiness} ₸
                </Text>
                {item.title === 'Daily' && discountDaily && (
                  <View style={{flexDirection: 'row', position: 'relative'}}>
                    <Text style={{fontSize: 18}}>
                      {basePriceDaily -
                        (discountDaily
                          ? (basePriceDaily * discountDaily) / 100
                          : 0
                        ).toFixed(0)}{' '}
                      ₸
                    </Text>
                    <Text
                      style={{
                        fontSize: 15,
                        color: 'green',
                        position: 'absolute',
                        top: -25,
                        left: -40,
                      }}>
                      {' '}
                      {discountDaily}%
                    </Text>
                  </View>
                )}
                {item.title === 'Comfort' && discountComfort && (
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontSize: 18}}>
                      {basePriceComfort -
                        (discountComfort
                          ? (basePriceComfort * discountComfort) / 100
                          : 0
                        ).toFixed(0)}{' '}
                      ₸
                    </Text>
                    <Text
                      style={{
                        fontSize: 15,
                        color: 'green',
                        position: 'absolute',
                        top: -25,
                        left: -40,
                      }}>
                      {' '}
                      {discountComfort}%
                    </Text>
                  </View>
                )}
                {item.title === 'Business' && discountBusiness && (
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontSize: 18}}>
                      {basePriceBusiness -
                        (discountBusiness
                          ? (basePriceBusiness * discountBusiness) / 100
                          : 0
                        ).toFixed(0)}{' '}
                      ₸
                    </Text>
                    <Text
                      style={{
                        fontSize: 15,
                        color: 'green',
                        position: 'absolute',
                        top: -25,
                        left: -40,
                      }}>
                      {' '}
                      {discountBusiness}%
                    </Text>
                  </View>
                )}
              </View>
            </TouchableOpacity>
          );
        })}
        {active && (
          <TouchableOpacity
            activeOpacity={0.7}
            style={{
              marginTop: 15,
              backgroundColor: '#222422',
              padding: 20,
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: '#fff', fontSize: 18}}>
              Order {active.title}
            </Text>
          </TouchableOpacity>
        )}
      </ScrollView>
    </View>
  );
});

export default RidePickUp;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  scrollWrap: {
    paddingHorizontal: '20@ms',
    paddingBottom: '25@ms',
  },
  headerWrap: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: screenHeight > 750 ? '15@ms' : '10@ms',
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: Platform.OS === 'android' ? '17@ms' : '16@ms',
    fontWeight: Platform.OS === 'android' ? 'bold' : '500',
    textAlign: 'center',
    width: '95%',
  },
});
