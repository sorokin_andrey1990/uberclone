import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {createStackNavigator} from '@react-navigation/stack';
import DropOffScreen from 'molecules/DropOffScreen';
import RidePickUp from 'molecules/RidePickUp';

const Stack = createStackNavigator();

const RideSelect = () => {
  return (
    <Stack.Navigator style={styles.container}>
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        style={{
          backgroundColor: '#fff',
        }}
        name={'DropOff'}
        component={DropOffScreen}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name={'RidePickUp'}
        component={RidePickUp}
      />
    </Stack.Navigator>
  );
};

export default RideSelect;

const styles = ScaledSheet.create({
  container: {},
});
