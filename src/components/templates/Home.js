import React from 'react';
import {View, Text, SafeAreaView, Image, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {SIZES} from 'constants';
import HomeButtons from 'molecules/HomeButtons';
import AutoComplete from 'molecules/AutoComplete';
import {observer} from 'mobx-react';
import {useTranslation} from 'react-i18next';
import Locations from 'store/locations';
import {toJS} from 'mobx';
import {handleAutoComplete} from 'services';

const Home = () => {
  const {t, i18n} = useTranslation();
  const handleField = (data, details) => {
    return Locations.setOrigin({
      location: details.geometry.location,
      description: data.description,
    });
  };

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={styles.logo} source={require('assets/uberLogo.png')} />
          <View style={{marginRight: 15}}>
            <TouchableOpacity
              onPress={() => {
                i18n.changeLanguage('ru');
              }}>
              <View>
                <Text>RU</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                i18n.changeLanguage('us');
              }}>
              <View>
                <Text>US</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <AutoComplete
          placeholder={t('HomePage.autoCompletePlaceholderOrigin')}
          handleAutoComplete={handleAutoComplete.setOrigin}
          currentLocationSet={handleAutoComplete.setCurrentLocation}
        />
        <HomeButtons />
      </View>
    </SafeAreaView>
  );
};

export default observer(Home);

const styles = ScaledSheet.create({
  container: {
    paddingHorizontal: SIZES.paddingMain,
  },
  logo: {
    width: '110@ms',
    height: '110@ms',
    resizeMode: 'contain',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
