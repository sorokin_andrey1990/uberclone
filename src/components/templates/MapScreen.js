import React, {useEffect, useRef, useMemo, useCallback} from 'react';
import {View, Text} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {ScaledSheet} from 'react-native-size-matters';
import Locations from 'store/locations';
import MapViewDirections from 'react-native-maps-directions';
import {observer} from 'mobx-react';
import AutoComplete from 'molecules/AutoComplete';
import {useTranslation} from 'react-i18next';
import handleAutoComplete from 'services/handleAutoComplete';
import MultiScreen from 'organisms/MultiScreen';

const MapScreen = () => {
  const mapRef = useRef(null);
  const {t} = useTranslation();

  useEffect(() => {
    if (!Locations.origin || !Locations.destination) return;
    console.log('FIT TO MARKRS');
    mapRef.current.fitToSuppliedMarkers(['origin', 'destination'], {
      edgePadding: {top: 50, left: 50, bottom: 50, right: 50},
    });
  }, [Locations.origin, Locations.destination]);
  console.log(Locations.origin);
  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1}}>
        <MapView
          ref={mapRef}
          provider="google"
          style={{width: '100%', height: '100%'}}
          region={{
            latitude: Locations.origin.location.lat,
            longitude: Locations.origin.location.lng,
            latitudeDelta: 0.008,
            longitudeDelta: 0.008,
          }}>
          {Locations.origin && (
            <Marker
              coordinate={{
                latitude: Locations.origin.location.lat,
                longitude: Locations.origin.location.lng,
              }}
              identifier="origin"
            />
          )}
          {Locations.destination && (
            <Marker
              coordinate={{
                latitude: Locations.destination.location.lat,
                longitude: Locations.destination.location.lng,
              }}
              identifier="destination"
            />
          )}
          {Locations.origin && Locations.destination && (
            <MapViewDirections
              origin={Locations.origin.description}
              destination={Locations.destination.description}
              apikey={'AIzaSyBv6C_P04-lb5dC9D0pD6170eR3ah0tpSo'}
              strokeWidth={5}
              strokeColor={'pink'}
            />
          )}
        </MapView>
      </View>
      {/* <View style={{flex: 1}}>
        <AutoComplete
          placeholder={t('HomePage.autoCompletePlaceholderDestination')}
          handleAutoComplete={handleAutoComplete.setDestination}
        />
      </View> */}
      <View style={{flex: 1}}>
        <MultiScreen />
      </View>
    </View>
  );
};

export default observer(MapScreen);

const styles = ScaledSheet.create({
  container: {},
});
