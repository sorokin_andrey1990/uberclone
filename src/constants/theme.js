import {Dimensions} from 'react-native';

export const SIZES = {
  paddingMain: '10@ms',
  radius5: '5@ms',
  padding10: '10@ms',
  titleText: '16@ms',
  margin5: '5@ms',
};

export const screenHeight = Dimensions.get('screen').height;
export const screenWidth = Dimensions.get('screen').width;

const theme = {SIZES, screenWidth, screenHeight};

export default theme;
