import {useTranslation} from 'react-i18next';

export default homeButtons = () => {
  const {t} = useTranslation();

  return [
    {
      id: 1,
      image: require('assets/UberX.png'),
      title: t('HomePage.pickRide'),
    },
    {
      id: 2,
      image: require('assets/delivery.png'),
      title: t('HomePage.delivery'),
    },
  ];
};
