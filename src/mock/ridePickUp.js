import {useTranslation} from 'react-i18next';

export default Rides = [
  {
    id: 1,
    image: require('assets/average.png'),
    //   title: t('HomePage.pickRide'),
    title: 'Daily',
  },
  {
    id: 2,
    image: require('assets/comfort.png'),
    title: 'Comfort',
  },
  {
    id: 3,
    image: require('assets/business.png'),
    title: 'Business',
  },
];
