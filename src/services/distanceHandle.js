import Locations from 'store/locations';
import {toJS} from 'mobx';

async function Distance() {
  console.log('From distanceHandle service', Locations);
  fetch(
    `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=${Locations.origin.description}
      &destinations=${Locations.destination.description}&key=AIzaSyBv6C_P04-lb5dC9D0pD6170eR3ah0tpSo`,
  )
    .then(res => res.json())
    .then(data => {
      console.log(data);
      Locations.setDistance(data.rows[0].elements[0].distance.text),
        Locations.setDuration(data.rows[0].elements[0].duration.text);
    });
}

export default Distance;
