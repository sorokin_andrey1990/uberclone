import Locations from 'store/locations';

class handleAutoComplete {
  setOrigin(data, details) {
    return Locations.setOrigin({
      location: details.geometry.location,
      description: data.description,
    });
  }

  setDestination(data, details) {
    return Locations.setDestination({
      location: details.geometry.location,
      description: data.description,
    });
  }
  setCurrentLocation(coords) {
    Locations.setOrigin({
      location: coords,
    });
  }
}

export default new handleAutoComplete();
