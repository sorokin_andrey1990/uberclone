function getAddressFromCoordinates(setAddress, reverseGeocode) {
  return new Promise(resolve => {
    const url = `https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=${reverseGeocode.lat}%2C${reverseGeocode.lng}%2C250&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=KZ4aDVjb4PjwcUb1ShzO-Mht5SsWBDyAQOmjVD3mdLM`;
    fetch(url)
      .then(res => res.json())
      .then(resJson => {
        if (
          resJson &&
          resJson.Response &&
          resJson.Response.View &&
          resJson.Response.View[0] &&
          resJson.Response.View[0].Result &&
          resJson.Response.View[0].Result[0]
        ) {
          resolve(
            setAddress(
              resJson.Response.View[0].Result[0].Location.Address.Street +
                ', ' +
                resJson.Response.View[0].Result[0].Location.Address
                  .HouseNumber +
                ', ' +
                resJson.Response.View[0].Result[0].Location.Address.City,
            ),
          );
        } else {
          resolve();
        }
      })
      .catch(e => {
        console.log('Error in getAddressFromCoordinates', e);
        resolve();
      });
  });
}

export default getAddressFromCoordinates;
