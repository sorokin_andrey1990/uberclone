import Locations from 'store/locations';
import {observer} from 'mobx-react';

class RidePrices {
  promo = null;
  baseRate = 100; // base 100
  comfortSurCharge = 1.2; // base 1.2
  businessSurCharge = 1.5; // base 1.5
  demandMultiplier = 1; // base 1
  discountDaily = null;
  discountComfort = null;
  discountBusiness = null;

  getDailyPrice(baseDistance) {
    const minDistance = !!baseDistance < 1;
    const maxDistance = !!baseDistance > 10;

    return (
      baseDistance *
      (this.baseRate * (minDistance ? 2 : maxDistance ? 1.2 : 1)) *
      this.demandMultiplier
    );
  }
  getComfortPrice(baseDistance) {
    const minDistance = !!baseDistance < 1;
    const maxDistance = !!baseDistance > 10;
    return (
      baseDistance *
      (this.baseRate * (minDistance ? 2 : maxDistance ? 1.2 : 1)) *
      this.comfortSurCharge *
      this.demandMultiplier
    );
  }

  getBusinessPrice(baseDistance) {
    const minDistance = !!baseDistance < 1;
    const maxDistance = !!baseDistance > 10;
    return (
      baseDistance *
      (this.baseRate * (minDistance ? 2 : maxDistance ? 1.2 : 1)) *
      this.businessSurCharge *
      this.demandMultiplier
    );
  }
}

export default new RidePrices();
