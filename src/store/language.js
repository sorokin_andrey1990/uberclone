import {makeAutoObservable} from 'mobx';
import {makePersistable} from 'mobx-persist-store';
import AsyncStorage from '@react-native-async-storage/async-storage';

class Language {
  language = '';

  constructor() {
    makeAutoObservable(this);
    makePersistable(this, {
      name: 'Language',
      properties: ['language'],
      storage: AsyncStorage,
    });
  }

  setLanguage(value) {
    this.language = value;
  }
}

export default new Language();
