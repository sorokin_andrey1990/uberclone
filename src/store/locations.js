import {makeAutoObservable} from 'mobx';

class Locations {
  origin = null;
  destination = null;
  distance = null;
  duration = null;

  constructor() {
    makeAutoObservable(this);
  }

  setOrigin(val) {
    this.origin = val;
  }

  setDestination(val) {
    this.destination = val;
  }

  setDistance(val) {
    console.log(val.split(' ')[0] * 1.609);
    this.distance = (val.split(' ')[0] * 1.60934).toFixed(2);
  }
  setDuration(val) {
    this.duration = val.split(' ')[0];
  }
}

export default new Locations();
