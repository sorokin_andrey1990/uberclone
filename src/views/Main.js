import React, {useEffect} from 'react';
import {ScaledSheet} from 'react-native-size-matters';
import {createStackNavigator} from '@react-navigation/stack';
import Home from 'templates/Home';
import MapScreen from 'templates/MapScreen';

const Stack = createStackNavigator();

const Main = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{headerShown: false}}
        name="Home"
        component={Home}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          // gestureEnabled: false
        }}
        name="MapScreen"
        component={MapScreen}
      />
    </Stack.Navigator>
  );
};

export default Main;

const styles = ScaledSheet.create({
  container: {},
});
